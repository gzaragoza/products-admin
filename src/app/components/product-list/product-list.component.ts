import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService, ProductsListInterface } from '../../services/products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  dataProducts: ProductsListInterface[] = [];

  displayedColumns: string[] = ['name', 'sku', 'barcode', 'enable'];
  size = 10;
  pageIndex = 0;
  dataSource: any;
  p = 0;

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];


  public productsListing: Array<ProductsListInterface>;

  constructor(private productService: ProductsService,
              private activateRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getDataFromApi()
    .subscribe(data => this.successData(data),
    err => this.failed(err));
  }

  successData(data: any) {
    this.dataProducts = data;
    this.dataSource = this.dataProducts.slice(0, 10);
  }

  failed(err: string) {
    console.log(err);
  }

  paginate(event: any) {
    this.pageIndex = event;
    this.dataSource = this.dataProducts
                      .slice(
                        event * this.size - this.size, event * this.size
                      );
  }

}
