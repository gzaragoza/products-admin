import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) {}

  getDataFromApi() {
    const URL_API = 'http://localhost:3000/products';
    return this.http.get<ProductsInterface>(URL_API);
  }
}

export interface ProductsInterface {
  id: number;
  name: string;
  sku: number;
  barcode: number;
  image: string;
  price: string;
  enable: boolean;
}

export interface ProductsListInterface {
  id: number;
  name: string;
  sku: number;
  barcode: number;
  image: string;
  enable: boolean;
}